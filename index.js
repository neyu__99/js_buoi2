// ex1
document.getElementById('TinhLuong').onclick=function(){
    var LuongNgay=Number(document.getElementById('LuongNgay').value)
    var SoNgay=Number(document.getElementById('SoNgay').value)

    var TongLuong=LuongNgay*SoNgay
    document.getElementById('TongLuong').innerHTML=` ${TongLuong.toLocaleString()} VND`
}
// ex2
document.getElementById('TinhTrungBinh').onclick=function(){
    var SoThu1=Number(document.getElementById('SoThu1').value)
    var SoThu2=Number(document.getElementById('SoThu2').value)
    var SoThu3=Number(document.getElementById('SoThu3').value)
    var SoThu4=Number(document.getElementById('SoThu4').value)
    var SoThu5=Number(document.getElementById('SoThu5').value)

    var GTTrungBinh=(SoThu1+SoThu2+SoThu3+SoThu4+SoThu5)/5
    document.getElementById('GTTrungBinh').innerHTML=GTTrungBinh
}
// ex3
document.getElementById('DoiTien').onclick=function(){
    var TienUSD=Number(document.getElementById('TienUSD').value)
    const USDrate=23500
    var TienVND=TienUSD*USDrate
    document.getElementById('TienVND').innerHTML=` ${TienVND.toLocaleString()} VND`
}
// ex4
document.getElementById('TinhHCN').onclick=function(){
    var ChieuDai=Number(document.getElementById('ChieuDai').value)
    var ChieuRong=Number(document.getElementById('ChieuRong').value)

    var ChuVi=(ChieuDai+ChieuRong)*2
    var DienTich=ChieuDai*ChieuRong
    document.getElementById('ChuviDientich').innerHTML=`Chu vi: ${ChuVi.toLocaleString()} ; Diện tích: ${DienTich.toLocaleString()}`
}
// ex5
document.getElementById('TinhTong').onclick=function(){
    var SoNhap=Number(document.getElementById('SoNhap').value)
    var chuc=Math.floor(SoNhap/10)
    console.log("🚀 ~ file: index.js ~ line 40 ~ document.getElementById ~ chuc", chuc)
    var donvi=SoNhap%10
    console.log("🚀 ~ file: index.js ~ line 42 ~ document.getElementById ~ donvi", donvi)
    var TongKyso=chuc+donvi
    document.getElementById('TongKyso').innerHTML=TongKyso
}
